import React, { useEffect, useState } from "react"
import classes from "./scrollArrow.module.scss"

const ScrollArrow = () => {
  const [showScroll, setShowScroll] = useState(false)

  useEffect(() => {
    window.addEventListener("scroll", checkScrollTop)
  }, [])

  const checkScrollTop = () => {
    if (!showScroll && window.pageYOffset > 400) {
      setShowScroll(true)
    } else if (showScroll && window.pageYOffset <= 400) {
      setShowScroll(false)
    }
  }

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" })
  }

  return (
    <div
      className={classes.scrollTop}
      onClick={scrollTop}
      style={{
        height: 40,
        display: showScroll ? "flex" : "none",
      }}
    >
      Top
    </div>
  )
}

export default ScrollArrow
