import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import Navbar from "react-bootstrap/Navbar"
import Nav from "react-bootstrap/Nav"
import NavItem from "react-bootstrap/NavItem"
import NavbarCollapse from "react-bootstrap/NavbarCollapse"
import NavbarToggle from "react-bootstrap/NavbarToggle"
import NavbarBrand from "react-bootstrap/NavbarBrand"

const Header = ({ siteTitle }) => (
  <header
    style={{
      background: `rebeccapurple`,
      marginBottom: `1.45rem`,
    }}
  >
    <Navbar bg="dark" expand="lg">
      <NavbarBrand>{siteTitle}</NavbarBrand>
      <NavbarToggle aria-controls="basic-navbar-nav" />
      <NavbarCollapse id="basic-navbar-nav">
        <Nav as="ul">
          <NavItem as="li">
            <Link to={"/"} className={"nav-link"}>
              Home
            </Link>
          </NavItem>
          <NavItem as="li">
            <Link to={"/cars"} className={"nav-link"}>
              Cars
            </Link>
          </NavItem>
          <NavItem as="li">
            <Link to={"/test"} className={"nav-link"}>
              Test
            </Link>
          </NavItem>
        </Nav>
      </NavbarCollapse>
    </Navbar>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
