import React from "react"
import Card from "react-bootstrap/Card"
import classes from "./carCard.module.scss"
import Badge from "react-bootstrap/Badge"

function CarCard(props) {
  return (
    <Card className={classes.carCard}>
      <Card.Body>
        <Card.Title
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <Badge variant="secondary" style={{ marginRight: "10px" }}>
            {props.id}
          </Badge>
          {props.title}
        </Card.Title>
        <Card.Subtitle className="mb-2 text-muted">
          {props.subtitle}
        </Card.Subtitle>
        <Card.Text>
          {props.carProperties.map(property => (
            <li key={property}>{property}</li>
          ))}
        </Card.Text>
      </Card.Body>
    </Card>
  )
}

export default CarCard
