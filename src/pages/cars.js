import React, { useEffect, useState } from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import CarCard from "../components/carCard"
import Container from "react-bootstrap/Container"
import Jumbotron from "react-bootstrap/Jumbotron"
import Badge from "react-bootstrap/Badge"
import Alert from "react-bootstrap/Alert"
import ScrollArrow from "../components/scrollArrow"

const CarPage = () => {
  const [error, setError] = useState(null)
  const [isLoaded, setIsLoaded] = useState(false)
  const [items, setItems] = useState([])

  useEffect(() => {
    if (isLoaded) return

    fetch("https://myfakeapi.com/api/cars/")
      .then(res => res.json())
      .then(
        result => {
          setIsLoaded(true)
          setItems(result.cars)
        },
        error => {
          setIsLoaded(true)
          setError(error)
        }
      )
  }, [])

  let content = null
  if (error) {
    content = (
      <Alert
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
        }}
        variant={"danger"}
      >
        <h1>Error: {error.message}</h1>
      </Alert>
    )
  } else if (!isLoaded) {
    content = (
      <Alert
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          width: "100%",
        }}
        variant={"warning"}
      >
        <h1>Loading..!</h1>
      </Alert>
    )
  } else {
    content = items.map((item, index) => (
      <CarCard
        key={item.id}
        id={item.id}
        title={item.car}
        subtitle={item.car_model}
        carProperties={[
          `Year: ${item.car_model_year}`,
          `Price: ${item.price}`,
          `Color: ${item.car_color}`,
          `Vin: ${item.car_vin}`,
        ]}
      ></CarCard>
    ))
  }

  return (
    <Layout>
      <SEO title="Cars" />
      <Container>
        <Jumbotron>
          <h1>
            <Badge variant="secondary">Cars</Badge>
          </h1>
          <p>List of Cars with some basic information about them.</p>
        </Jumbotron>{" "}
        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          {content}
        </div>
      </Container>
      <Link to="/">Go back to the homepage</Link>
      <ScrollArrow></ScrollArrow>
    </Layout>
  )
}

export default CarPage
