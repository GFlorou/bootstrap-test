import React from "react"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import classes from "../Styles/index.module.scss"
import Container from "react-bootstrap/Container"
import Jumbotron from "react-bootstrap/Jumbotron"
import Badge from "react-bootstrap/Badge"
import ScrollArrow from "../components/scrollArrow"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" description="This is a website constructed with ReactJs and Gatsby for the purpose
          of testing the boostrap css framework to determine if it is
          appropriate for the upcoming project." />
    <Container className={classes.Index}>
      <Jumbotron>
        <h1>
          <Badge variant="secondary">Hello visitor</Badge>
        </h1>
        <p>
          This is a website constructed with ReactJs and Gatsby for the purpose
          of testing the boostrap css framework to determine if it is
          appropriate for the upcoming project.
        </p>
      </Jumbotron>
      <div className={classes.ImageContainer}>
        <Image />
      </div>
    </Container>
    <ScrollArrow></ScrollArrow>
  </Layout>
)

export default IndexPage
