import React, { useState } from "react"

import Jumbotron from "react-bootstrap/Jumbotron"
import Container from "react-bootstrap/Container"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import Media from "react-bootstrap/Media"
import ToastComponent from "../components/toast"
import Layout from "../components/layout"
import Alert from "react-bootstrap/Alert"
import Accordion from "react-bootstrap/Accordion"
import Card from "react-bootstrap/Card"
import Button from "react-bootstrap/Button"
import Modal from "react-bootstrap/Modal"
import Badge from "react-bootstrap/Badge"
import ModalComponent from "../components/modalComponent"
import HoverTooltipComponent from "../components/hoverTooltipComponent"
import PopoverComponent from "../components/popoverComponent"
import ProgressBar from "react-bootstrap/ProgressBar"
import Spinner from "react-bootstrap/Spinner"
import Tabs from "react-bootstrap/Tabs"
import Tab from "react-bootstrap/Tab"
import Table from "react-bootstrap/Table"

const colStyle = { backgroundColor: "gray", border: "1px solid black" }

const TestPage = () => (
  <Layout>
    <Container className="p-3">
      <Jumbotron>
        <h1 className="header">Welcome To React-Bootstrap</h1>
        <ToastComponent>
          We now have Toasts
          <span role="img" aria-label="tada">
            🎉
          </span>
        </ToastComponent>
      </Jumbotron>
      <Container>
        <Row>
          <Col sm={8} style={colStyle}>
            sm=8
            <Media>
              <img
                width={64}
                height={64}
                className="mr-3"
                src="holder.js/64x64"
                alt="Generic placeholder"
              />
              <Media.Body>
                <h5>Media Heading</h5>
                <p>
                  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                  scelerisque ante sollicitudin commodo. Cras purus odio,
                  vestibulum in vulputate at, tempus viverra turpis. Fusce
                  condimentum nunc ac nisi vulputate fringilla. Donec lacinia
                  congue felis in faucibus.
                </p>

                <Media>
                  <img
                    width={64}
                    height={64}
                    className="mr-3"
                    src="holder.js/64x64"
                    alt="Generic placeholder"
                  />
                  <Media.Body>
                    <h5>Media Heading</h5>
                    <p>
                      Cras sit amet nibh libero, in gravida nulla. Nulla vel
                      metus scelerisque ante sollicitudin commodo. Cras purus
                      odio, vestibulum in vulputate at, tempus viverra turpis.
                      Fusce condimentum nunc ac nisi vulputate fringilla. Donec
                      lacinia congue felis in faucibus.
                    </p>
                  </Media.Body>
                </Media>
              </Media.Body>
            </Media>
          </Col>
          <Col sm={4} style={colStyle}>
            sm=4
            {[
              "primary",
              "secondary",
              "success",
              "danger",
              "warning",
              "info",
              "light",
              "dark",
            ].map((variant, idx) => (
              <Alert key={idx} variant={variant}>
                This is a {variant} alert with{" "}
                <Alert.Link href="#">an example link</Alert.Link>. Give it a
                click if you like.
              </Alert>
            ))}
          </Col>
        </Row>
        <Row>
          <Col sm style={colStyle}>
            sm=true
            {
              <Accordion defaultActiveKey="0">
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                      Click me!
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>Hello! I'm the body</Card.Body>
                  </Accordion.Collapse>
                </Card>
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="1">
                      Click me!
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="1">
                    <Card.Body>Hello! I'm another body</Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
            }
          </Col>
          <Col sm style={colStyle}>
            sm=true
            <div>
              <h1>
                Example heading <Badge variant="secondary">New</Badge>
              </h1>
              <h2>
                Example heading <Badge variant="secondary">New</Badge>
              </h2>
              <h3>
                Example heading <Badge variant="secondary">New</Badge>
              </h3>
              <h4>
                Example heading <Badge variant="secondary">New</Badge>
              </h4>
              <h5>
                Example heading <Badge variant="secondary">New</Badge>
              </h5>
              <h6>
                Example heading <Badge variant="secondary">New</Badge>
              </h6>
            </div>
          </Col>
          <Col sm style={colStyle}>
            sm=true
            <ModalComponent></ModalComponent>
          </Col>
        </Row>
        <Row>
          <Col sm={8} style={colStyle}>
            sm=8
            <HoverTooltipComponent></HoverTooltipComponent>
          </Col>
          <Col sm={4} style={colStyle}>
            sm=4
            <PopoverComponent></PopoverComponent>
          </Col>
        </Row>
        <Row>
          <Col sm={8} style={colStyle}>
            sm=8
            <div>
              <ProgressBar variant="success" now={40} />
              <ProgressBar variant="info" now={20} />
              <ProgressBar variant="warning" now={60} />
              <ProgressBar variant="danger" now={80} />
              <ProgressBar animated now={45} />
              <ProgressBar striped variant="success" now={35} key={1} />
              <ProgressBar variant="warning" now={20} key={2} />
              <ProgressBar striped variant="danger" now={10} key={3} />
            </div>
          </Col>
          <Col sm={4} style={colStyle}>
            sm=4
            <Spinner animation="border" role="status">
              <span className="sr-only">Loading...</span>
            </Spinner>
            <Spinner animation="grow" />
            <Spinner animation="border" variant="primary" />
            <Spinner animation="border" variant="secondary" />
            <Spinner animation="border" variant="success" />
            <Spinner animation="border" variant="danger" />
            <Spinner animation="border" variant="warning" />
            <Spinner animation="border" variant="info" />
            <Spinner animation="border" variant="light" />
            <Spinner animation="border" variant="dark" />
            <Spinner animation="grow" variant="primary" />
            <Spinner animation="grow" variant="secondary" />
            <Spinner animation="grow" variant="success" />
            <Spinner animation="grow" variant="danger" />
            <Spinner animation="grow" variant="warning" />
            <Spinner animation="grow" variant="info" />
            <Spinner animation="grow" variant="light" />
            <Spinner animation="grow" variant="dark" />
            <Button variant="primary" disabled>
              <Spinner
                as="span"
                animation="border"
                size="sm"
                role="status"
                aria-hidden="true"
              />
              <span className="sr-only">Loading...</span>
            </Button>
          </Col>
        </Row>{" "}
        <Row>
          <Col sm={8} style={colStyle}>
            sm=8
            <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
              <Tab eventKey="home" title="Home">
                That thou hast her it is not all my grief, And yet it may be
                said I loved her dearly; That she hath thee is of my wailing
                chief, A loss in love that touches me more nearly. Loving
                offenders thus I will excuse ye: Thou dost love her, because
                thou know'st I love her; And for my sake even so doth she abuse
                me, Suffering my friend for my sake to approve her. If I lose
                thee, my loss is my love's gain, And losing her, my friend hath
                found that loss;{" "}
              </Tab>
              <Tab eventKey="profile" title="Profile">
                That thou hast her it is not all my grief, And yet it may be
                said I loved her dearly; That she hath thee is of my wailing
                chief, A loss in love that touches me more nearly. Loving
                offenders thus I will excuse ye: Thou dost love her, because
                thou know'st I love her; And for my sake even so doth she abuse
                me, Suffering my friend for my sake to approve her. If I lose
                thee, my loss is my love's gain, And losing her, my friend hath
                found that loss;{" "}
              </Tab>
              <Tab eventKey="contact" title="Contact" disabled>
                That thou hast her it is not all my grief, And yet it may be
                said I loved her dearly; That she hath thee is of my wailing
                chief, A loss in love that touches me more nearly. Loving
                offenders thus I will excuse ye: Thou dost love her, because
                thou know'st I love her; And for my sake even so doth she abuse
                me, Suffering my friend for my sake to approve her. If I lose
                thee, my loss is my love's gain, And losing her, my friend hath
                found that loss;{" "}
              </Tab>
            </Tabs>
          </Col>
          <Col sm={4} style={colStyle}>
            sm=4
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Username</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Jacob</td>
                  <td>Thornton</td>
                  <td>@fat</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td colSpan="2">Larry the Bird</td>
                  <td>@twitter</td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </Container>
  </Layout>
)

export default TestPage
